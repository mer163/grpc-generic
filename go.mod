module grpc-generic

go 1.20

require (
	github.com/golang/protobuf v1.5.3
	github.com/jhump/protoreflect v1.15.5
	github.com/sirupsen/logrus v1.9.3
	google.golang.org/grpc v1.61.0
	google.golang.org/protobuf v1.31.1-0.20231027082548-f4a6c1f6e5c1
)

require (
	github.com/bufbuild/protocompile v0.8.0 // indirect
	golang.org/x/net v0.18.0 // indirect
	golang.org/x/sync v0.5.0 // indirect
	golang.org/x/sys v0.14.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20231106174013-bbf56f31fb17 // indirect
)
