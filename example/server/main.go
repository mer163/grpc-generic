package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	hello2 "grpc-generic/example/api/hello"
	"net"
)

func main() {
	server := grpc.NewServer()
	// 注册反射服务
	reflection.Register(server)
	hello2.RegisterHelloServer(server, new(HelloServer))

	listen, err := net.Listen("tcp", ":8888")
	if err != nil {
		panic(err)
	}

	err = server.Serve(listen)
	if err != nil {
		panic(err)
	}
}

type HelloServer struct {
	hello2.UnimplementedHelloServer
}

func (h *HelloServer) SayHello(ctx context.Context, req *hello2.HelloReq) (*hello2.HelloResp, error) {
	fmt.Printf("receive: %s, %s\n", req.Uid, req.Message)
	res := &hello2.HelloResp{
		Message: fmt.Sprintf("received %s", req.Message),
	}
	return res, nil
}
