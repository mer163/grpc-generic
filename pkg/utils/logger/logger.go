package logger

import (
	"github.com/sirupsen/logrus"
)

var Logger *SoLogger

func init() {
	Logger = &SoLogger{logger: logrus.StandardLogger()}
}

type SoLogger struct {
	logger *logrus.Logger
}

func (l *SoLogger) Info(args ...any) {
	l.logger.Info(args...)
}

func (l *SoLogger) Infoln(args ...any) {
	l.logger.Infoln(args...)
}

func (l *SoLogger) Infof(format string, args ...any) {
	l.logger.Infof(format, args...)
}

func (l *SoLogger) Warning(args ...any) {
	l.logger.Warning(args...)
}

func (l *SoLogger) Warningln(args ...any) {
	l.logger.Warningln(args...)
}

func (l *SoLogger) Warningf(format string, args ...any) {
	l.logger.Warningf(format, args...)
}

func (l *SoLogger) Error(args ...any) {
	l.logger.Error(args...)
}

func (l *SoLogger) Errorln(args ...any) {
	l.logger.Errorln(args...)
}

func (l *SoLogger) Errorf(format string, args ...any) {
	l.logger.Errorf(format, args...)
}

func (l *SoLogger) Fatal(args ...any) {
	l.logger.Fatal(args...)
}

func (l *SoLogger) Fatalln(args ...any) {
	l.logger.Fatalln(args...)
}

func (l *SoLogger) Fatalf(format string, args ...any) {
	l.logger.Fatalf(format, args...)
}

var (
	logrusLevels    = []logrus.Level{logrus.TraceLevel, logrus.DebugLevel, logrus.InfoLevel, logrus.WarnLevel, logrus.ErrorLevel, logrus.FatalLevel, logrus.PanicLevel}
	logrusLevelsLen = len(logrusLevels)
)

// V enable level
// grpclog:info=2,warn=3...
// logrus: warn=3,info=4...
func (l *SoLogger) V(level int) bool {
	if level >= logrusLevelsLen {
		return true
	}
	return logrusLevels[level] <= l.logger.Level
}
