package logger

func Info(args ...any) {
	Logger.Info(args...)
}

func Infoln(args ...any) {
	Logger.Infoln(args...)
}

func Infof(format string, args ...any) {
	Logger.Infof(format, args...)
}

func Warning(args ...any) {
	Logger.Warning(args...)
}

func Warningln(args ...any) {
	Logger.Warningln(args...)
}

func Warningf(format string, args ...any) {
	Logger.Warningf(format, args...)
}

func Error(args ...any) {
	Logger.Error(args...)
}

func Errorln(args ...any) {
	Logger.Errorln(args...)
}

func Errorf(format string, args ...any) {
	Logger.Errorf(format, args...)
}

func Fatal(args ...any) {
	Logger.Fatal(args...)
}

func Fatalln(args ...any) {
	Logger.Fatalln(args...)
}

func Fatalf(format string, args ...any) {
	Logger.Fatalf(format, args...)
}
