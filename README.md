
[**完整文档：Golang GRPC泛化调用**](http://localhost:4000/2023/11/10/article/Golang%20Grpc%E6%B3%9B%E5%8C%96%E8%B0%83%E7%94%A8/)

**请查看 example 目录下的 server/client 泛化调用示例。**

example 中实现了客户端泛化调用代码，json 格式和 protobuf 格式参数的一元 RPC 泛化调用示例，Server Stream、Client Stream、与双向流 RPC 通信模式可以根据需要参考实现。

Grpc 为了实现跨语言特性，通过中间语言 IDL 进行了服务和数据类型的定义，写在 .proto 文件中。由 proto 代码生成工具根据 idl 文件生成各目标语言的 grpc 服务端和客户端代码。

正常调用 grpc 服务必须使用生成出的客户端代码进行调用。

当前业务需要使用网关对 grpc 请求进行代理，统一处理 grpc 客户端请求流量，**若每次 grpc 服务新增或修改后，都需重新引入客户端调用代码重启网关，会异常麻烦加大开发运维成本。**

这里使用 grpc 泛化调用解决该问题，只提供服务名、方法名和请求参数数据(json或protobuf格式)，即可调用 grpc 服务。

## 泛化调用应用场景

### 服务网关 gateway
客户端通常使用 http 和 websocket 请求进行访问，在网管层可以使用泛化调用技术将非grpc请求代理访问后端 grpc 服务

![grpc generic gateway](http://www.tangmingyou.com/img/article/grpc_generic/grpc_generic1.png)

- 在 http 请求中，可以将 grpc 服务名和方法放置在 url 或请求体中，grpc 请求参数使用 json 请求体
- 在 websocket 中，可选择直接使用 protobuf 序列化的请求体，减少请求流量大小

### grpc 调试测试工具
如 apifox 类文档工具都提供了 grpc 服务调用功能

[grpc-client-cli 工具可使用命令行对 grpc 服务调用](https://github.com/vadimi/grpc-client-cli) （grpc服务端需开启反射服务或提供proto文件）

## grpc 反射服务

gRPC 提供了 `grpc.reflection.v1alpha.ServerReflection` 服务，在 Server 端添加后可以通过该服务获取所有服务的信息，包括服务定义，方法，属性等；

服务端开启反射服务：
```golang
func main() {
	server := grpc.NewServer()
	// 注册反射服务
	reflection.Register(server)
	hello.RegisterHelloServer(server, new(HelloServer))

	listen, err := net.Listen("tcp", ":8888")
	if err != nil {
		panic(err)
	}

	err = server.Serve(listen)
	if err != nil {
		panic(err)
	}
}
```

[核心实现使用社区开源的 grpc stub 反射访问库 jhump/protoreflect](https://github.com/jhump/protoreflect)
